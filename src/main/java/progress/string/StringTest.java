package progress.string;

/**
 * @Classname StringTest
 * @Description TODO
 * @Date 2020/12/8 17:18
 * @Created by 张乃文
 */
public class StringTest {
   private  static int length = 1000 * 100;

    //String长度大小结果
    //受JVM堆内存参数大小限制和方法区剩余大小限制-Xmx256m

    public static void main(String[] args) {
        //String长度测试
        testLength();
        //字符串+=性能测试（相差百倍 ）
        testBuilderAdding();
        //StringBuilder append性能测试
        testOrdinaryAdding();


    }

    public  static  int testLength(){
        StringBuffer params = new StringBuffer();
        for (int i = 1; i <= length ; i++) {
            params.append("我");
        }
        return params.toString().length();
    }
    public static  long testBuilderAdding(){
        StringBuilder builder = new StringBuilder();
        long l1 = System.currentTimeMillis();
        for (int i = 1; i <= length ; i++) {
            builder.append("我");
        }
        long l2 = System.currentTimeMillis();
        long l3 = l2-l1;
        return l3;
    }
    public static  long testOrdinaryAdding(){
        String  str = null;
        long l4 = System.currentTimeMillis();
        for (int i = 1; i <= length ; i++) {
            str += "是";
        }
        long l5 = System.currentTimeMillis();
        long l6 = l5 - l4;
        return l6;
    }
}
