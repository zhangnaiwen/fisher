package progress.string;

import com.google.common.base.Strings;
import org.springframework.util.StringUtils;

/**
 * @ClassName DesensitizedUtil
 * @Description: TODO
 * @Author wangs
 * @Date 2020/12/12
 * @Version V1.0
 **/
public class DesensitizedUtil {
    public static void main(String[] args) {
        String idNumber =  desensitizedIdNumber("220581199201171672");
        String phoneNumber =  desensitizedPhoneNumber("17519469258");
        System.out.println(idNumber);
        System.out.println(phoneNumber);
    }
    /**
     * 手机号脱敏
     * @param phoneNumber
     * @return
     */
    public static String desensitizedPhoneNumber(String phoneNumber){
        if(!StringUtils.isEmpty(phoneNumber)){
            phoneNumber = phoneNumber.replaceAll("(\\w{3})\\w*(\\w{4})", "$1****$2");
        }
        return phoneNumber;
    }

    /**
     * 身份证脱敏
     * @param idNumber
     * @return
     */
    public static String desensitizedIdNumber(String idNumber){
        if (!Strings.isNullOrEmpty(idNumber)) {
            if (idNumber.length() == 15){
                idNumber = idNumber.replaceAll("(\\w{6})\\w*(\\w{3})", "$1******$2");
            }
            if (idNumber.length() == 18){
                idNumber = idNumber.replaceAll("(\\w{6})\\w*(\\w{3})", "$1*********$2");
            }
        }
        return idNumber;
    }
}
