package progress.momo;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;

/**
 ** mongodb删除测试
 *  @Author: ZNW
 *  @Date:2020/7/21-20:56
 */
public class MongoDelete {

    public static void main(String[] args) {
        //获取数据库连接对象
        MongoDatabase mongoDatabase = MongoUtil.getConnect();
        //获取集合
        MongoCollection<Document> collection = mongoDatabase.getCollection("myCol");
        //申明删除条件
        Bson filter = Filters.eq("age",18);
        //删除与筛选器匹配的单个文档
        collection.deleteOne(filter);
        //删除与筛选器匹配的多个文档
        //collection.deleteMany(filter);
    }
}
