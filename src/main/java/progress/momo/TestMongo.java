package progress.momo;



import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class TestMongo {


    public static void main(String[] args) {
        MongoCollection<Document> collection = MongoClients.create("mongodb://admin:gk123kg@8.131.51.198:27017/").
                getDatabase("dev_zhuowang_cert").
                getCollection("biz_cert_info");


        Document document = new Document();
        document.put("certSerialNumber","GDSFDS");
        document.put("certSubject",43214);
        document.put("certIssuer",6452);
        document.put("certNotBefore", LocalDateTime.now().toString());
        document.put("certNotAfter", LocalDateTime.now().toString());
        document.put("createBy","admin");
        document.put("delFlag","0");
        document.put("userPhone","17519469253");
        document.put("userIdNo","220581199201171672");
        document.put("userName","张乃文");
        document.put("certStatus",1);
        document.put("certApplySource",1);
        document.put("certType",1);
        document.put("certLevel",1);
        collection.insertOne(document);
    }
}
