package progress.momo;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 ** 陌陌安全开源了 Java 静态代码安全审计插件 MOMO Code Sec Inspector
 *  侧重于在编码过程中发现项目潜在的安全风险，并提供一键修复能力
 *  @Author: ZNW
 *  @Date:2020/7/21-20:56
 */
public class  InspectorTest {

    public void build() throws ParserConfigurationException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
    }
}
