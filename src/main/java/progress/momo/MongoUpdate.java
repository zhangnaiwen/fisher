package progress.momo;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;

/**
 ** mongodb修改测试
 *  @Author: ZNW
 *  @Date:2020/7/21-20:56
 */
public class MongoUpdate {

    public static void main(String[] args) {
        //获取数据库连接对象
        MongoDatabase mongoDatabase = MongoUtil.getConnect();
        //获取集合
        MongoCollection<Document> collection = mongoDatabase.getCollection("myCol");
        //修改过滤器
        Bson filter = Filters.eq("name", "张三");
        //指定修改的更新文档
        Document document = new Document("$set", new Document("age", 100));
        //修改单个文档
        collection.updateOne(filter, document);
        //修改多个文档
        //collection.updateMany(filter, document);
    }
}
