package progress.momo;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import java.util.ArrayList;
import java.util.List;

/**
 ** mongodb连接工具类
 *  @Author: ZNW
 *  @Date:2020/7/21-20:56
 */
public class MongoUtil {

    public  static  String SERVER = "127.0.0.1";
    public  static  int PORT = 27017;
    public  static  String USER = "root";
    public  static  char[] PASSWORD = "root".toCharArray();
    public  static  String DATABASE = "zhuowang_cert";

    /**
     * @Method:getConnect
     * @Author: ZNW
     * @Date:2020/11/17-19:47
     * @Describe:需要密码认证方式连接
     * @Params:[]
     * @Return:com.mongodb.client.MongoDatabase
     */
    public  static MongoDatabase getConnect(){
        //Mongodb连接的服务地址、端口
        List<ServerAddress> serverAddresses = new ArrayList<>();
        ServerAddress serverAddress = new ServerAddress(SERVER, PORT);
        serverAddresses.add(serverAddress);

        //Mongodb认证的用户名、数据库、密码
        List<MongoCredential> credentials = new ArrayList<>();
        MongoCredential credential = MongoCredential.createScramSha1Credential(USER, DATABASE, PASSWORD);
        credentials.add(credential);

        //通过连接认证获取MongoDB连接
        MongoClient mongoClient = new MongoClient(serverAddresses, credentials);

        //连接到数据库
        MongoDatabase mongoDatabase = mongoClient.getDatabase(DATABASE);

        //返回连接数据库对象
        return mongoDatabase;
    }



}
