package progress.momo;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;

/**
 ** mongodb查询测试
 *  @Author: ZNW
 *  @Date:2020/7/21-20:56
 */
public class MongoFind {

    public static void main(String[] args) {
        //获取数据库连接对象
        MongoDatabase mongoDatabase = MongoUtil.getConnect();
        //获取集合
        MongoCollection<Document> collection = mongoDatabase.getCollection("myCol");
        //指定查询过滤器
        Bson filter = Filters.eq("name", "张三");

        //查找集合中的所有文档
        FindIterable findIterable = collection.find(filter);
        MongoCursor cursor = findIterable.iterator();
        while (cursor.hasNext()) {
            System.out.println("Mongo结果"+cursor.next());
        }
    }
}
