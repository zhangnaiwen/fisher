package progress.momo;



import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import java.util.ArrayList;
import java.util.List;

/**
 ** mongodb新增测试
 *  @Author: ZNW
 *  @Date:2020/7/21-20:56
 */
public class MongoInsert {

    public static void main(String[] args) {
        //insertOneTest();
        //insertManyTest();
    }
    /**
     * @Method:insertOneTest
     * @Author: ZNW
     * @Date:2020/11/17-19:47
     * @Describe:新增一个DOCUMENT
     * @Params:[]
     * @Return:void
     */
    public static  void insertOneTest(){
        //获取数据库连接对象
        MongoDatabase mongoDatabase = MongoUtil.getConnect();
        //获取集合
        MongoCollection<Document> collection = mongoDatabase.getCollection("myCol");
        //要插入的数据
        Document document = new Document("name","张三")
                .append("sex", "男")
                .append("age", 18);
        //插入一个文档
        collection.insertOne(document);
    }
    /**
     * @Method:insertManyTest
     * @Author: ZNW
     * @Date:2020/11/17-19:47
     * @Describe:测试新增多个DOCUMENT
     * @Params:[]
     * @Return:void
     */
    public static  void insertManyTest(){
        //获取数据库连接对象
        MongoDatabase mongoDatabase = MongoUtil.getConnect();
        //获取集合
        MongoCollection<Document> collection = mongoDatabase.getCollection("myCol");
        //要插入的数据
        List<Document> list = new ArrayList<>();
        for(int i = 18; i <= 21; i++) {
            Document document = new Document("name", "张三"+i)
                    .append("sex", "男")
                    .append("age", i);
            list.add(document);
        }
        //插入多个文档
        collection.insertMany(list);
    }


 }
