package progress.lru;

public class LruTest {
    public static void main(String[] args) {

        LruCache<Integer,String> lru = new LruCache<Integer,String>(3);

        lru.put(1, "a");
        System.out.println(lru.toString());

        lru.put(2, "b");
        System.out.println(lru.toString());

        lru.put(3, "c");
        System.out.println(lru.toString());

        lru.put(4, "d");
        System.out.println(lru.toString());

        lru.put(1, "aa");
        System.out.println(lru.toString());

        lru.put(2, "bb");
        System.out.println(lru.toString());

        lru.put(5, "e");
        System.out.println(lru.toString());

        lru.get(1);
        System.out.println(lru.toString());

        lru.remove(11);
        System.out.println(lru.toString());

        lru.remove(1);
        System.out.println(lru.toString());

        lru.put(1, "aaa");
        System.out.println(lru.toString());
    }
}
