package progress.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @Classname FileUtils
 * @Description TODO
 * @Date 2020/12/16 8:55
 * @Created by 张乃文
 */
public class FileUtil  {

    public static File multipartFile2File(MultipartFile multipartFile) throws Exception {

        File file = null;
        if (multipartFile == null || multipartFile.getSize() == 0) {
            file = null;
        } else {
            InputStream ins = null;
            ins = multipartFile.getInputStream();
            file = new File(multipartFile.getOriginalFilename());
            try {
                OutputStream os = new FileOutputStream(file);
                int bytesRead = 0;
                byte[] buffer = new byte[8192];
                while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
                    os.write(buffer, 0, bytesRead);
                }
                os.close();
                ins.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            ins.close();
        }
        return file;
    }

}
