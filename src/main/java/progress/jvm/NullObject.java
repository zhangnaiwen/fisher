package progress.jvm;

import org.openjdk.jol.info.ClassLayout;

/**
 * 测试空对象会占用多少字节
 * @Author: ZNW
 * @Date:2020/8/2-18:20
 */
public class NullObject {
    public static void main(String[] args) {
        System.out.println(ClassLayout.parseInstance(new NullObject()).toPrintable());
    }
}
