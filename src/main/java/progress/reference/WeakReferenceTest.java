package progress.reference;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;


/**
 *当JVM进行垃圾回收时，无论内存是否充足，都会回收被弱引用关联的对象。
 *这说明只要JVM进行垃圾回收，被弱引用关联的对象必定会被回收掉。
 *不过要注意的是，这里所说的被弱引用关联的对象是指只有弱引用与之关联，
 *如果存在强引用同时与之关联，则进行垃圾回收时也不会回收该对象
 */
public class WeakReferenceTest {
    public static void main(String[] args) {
        ReferenceQueue<String> queue = new ReferenceQueue<String>();
        //弱引用
        WeakReference<String> weakReference = new WeakReference<String>(new String("hello"), queue);
        System.out.println(weakReference.get());
        System.gc();
        System.out.println(weakReference.get());
    }
}
