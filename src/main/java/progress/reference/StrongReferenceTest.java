package progress.reference;

/**
 * 只要某个对象有强引用与之关联，JVM必定不会回收这个对象，
 * 即使在内存不足的情况下，JVM宁愿抛出OutOfMemory错误也不会回收这种对象
 * 如果想中断强引用和某个对象之间的关联，可以显示地将引用赋值为null，这样一来的话，JVM在合适的时间就会回收该对象
 */
public class StrongReferenceTest {
    public static void main(String[] args) {
        Object object = new Object();
        String str = "hello";
    }
}
