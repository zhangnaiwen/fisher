package progress.curve;

import java.math.BigInteger;

/**
 * @Author: ERIC
 * @Date:2020/7/17-22:56
 * @Describe:椭圆曲线的基类
 */
public abstract class EncryptionCurve {
	BigInteger q;
	EncryptionFieldElement a, b;

	public EncryptionCurve(BigInteger q, BigInteger a, BigInteger b) {
		this.q = q;
		this.a = fromBigInteger(a);
		this.b = fromBigInteger(b);
	}

	public abstract EncryptionFieldElement fromBigInteger(BigInteger x);

	public abstract EncryptionPoint decodePoint(byte[] encoded);

    public EncryptionFieldElement getA()
    {
        return a;
    }

    public EncryptionFieldElement getB()
    {
        return b;
    }

    /**
     * Elliptic curve over Fp
     */
    public static class Fp extends EncryptionCurve {
        public Fp(BigInteger q, BigInteger a, BigInteger b)
        {
            super(q, a, b);
        }

        public BigInteger getQ()
        {
            return q;
        }

        @Override
        public EncryptionFieldElement fromBigInteger(BigInteger x) {
            return new EncryptionFieldElement.Fp(this.q, x);
        }

        /**
         * decode a point on this curve which has been encoded using
         * point compression (X9.62 s 4.2.1 pg 17) returning the point.
         */
        @Override
        public EncryptionPoint decodePoint(byte[] encoded) {
            EncryptionPoint p = null;

            switch (encoded[0])
            {
				// compressed
			case 0x02:
			case 0x03:
                int ytilde = encoded[0] & 1;
                byte[]  i = new byte[encoded.length - 1];

                System.arraycopy(encoded, 1, i, 0, i.length);

                EncryptionFieldElement x = new EncryptionFieldElement.Fp(this.q, new BigInteger(1, i));
                EncryptionFieldElement alpha = x.multiply(x.square()).add(x.multiply(a).add(b));
                EncryptionFieldElement beta = alpha.sqrt();

                //
                // if we can't find a sqrt we haven't got a point on the
                // curve - run!
                //
                if (beta == null) {
                    throw new RuntimeException("Invalid point compression");
                }

                int bit0 = (beta.toBigInteger().testBit(0) ? 0 : 1);

                if ( bit0 == ytilde ) {
                    p = new EncryptionPoint.Fp(this, x, beta);
                } else {
                    p = new EncryptionPoint.Fp(this, x,
                        new EncryptionFieldElement.Fp(this.q, q.subtract(beta.toBigInteger())));
                }
                break;
            case 0x04:
                byte[]  xEnc = new byte[(encoded.length - 1) / 2];
                byte[]  yEnc = new byte[(encoded.length - 1) / 2];

                System.arraycopy(encoded, 1, xEnc, 0, xEnc.length);
                System.arraycopy(encoded, xEnc.length + 1, yEnc, 0, yEnc.length);

                p = new EncryptionPoint.Fp(this,
                        new EncryptionFieldElement.Fp(this.q, new BigInteger(1, xEnc)),
                        new EncryptionFieldElement.Fp(this.q, new BigInteger(1, yEnc)));
                break;
            default:
                throw new RuntimeException("Invalid point encoding 0x" + Integer.toString(encoded[0], 16));
            }

            return p;
        }
    }
}
