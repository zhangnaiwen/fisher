package progress.curve;

import java.math.BigInteger;


/**
 * @Author: ERIC
 * @Date:2020/7/17-22:56
 * @Describe:椭圆曲线的元素
 */
public abstract class EncryptionFieldElement implements EncryptionConstants {
	BigInteger x;
	BigInteger p;

	protected EncryptionFieldElement(BigInteger q, BigInteger x) {
        if (x.compareTo(q) >= 0) {
            throw new IllegalArgumentException("x value too large in field element");
        }

		this.x = x;
		this.p = q;
	}

	public BigInteger toBigInteger() {
		return x;
	}
    @Override
	public boolean equals(Object other) {
		if (other == this) {
            return true;
        }
		if (!(other instanceof EncryptionFieldElement)) {
            return false;
        }
		EncryptionFieldElement o = (EncryptionFieldElement)other;
		return p.equals(o.p) && x.equals(o.x);
	}

	public abstract String         getFieldName();
	public abstract EncryptionFieldElement add(EncryptionFieldElement b);
	public abstract EncryptionFieldElement subtract(EncryptionFieldElement b);
	public abstract EncryptionFieldElement multiply(EncryptionFieldElement b);
	public abstract EncryptionFieldElement divide(EncryptionFieldElement b);
	public abstract EncryptionFieldElement negate();
	public abstract EncryptionFieldElement square();
	public abstract EncryptionFieldElement invert();
	public abstract EncryptionFieldElement sqrt();

    public static class Fp extends EncryptionFieldElement {
        /**
         * return the field name for this field.
         *
         * @return the string "Fp".
         */
        @Override
        public String getFieldName()
        {
            return "Fp";
        }

        public Fp(BigInteger q, BigInteger x)
        {
            super(q, x);
        }

        @Override
        public EncryptionFieldElement add(EncryptionFieldElement b)
        {
            return new Fp(p, x.add(b.x).mod(p));
        }

        @Override
        public EncryptionFieldElement subtract(EncryptionFieldElement b)
        {
            return new Fp(p, x.subtract(b.x).mod(p));
        }

        @Override
        public EncryptionFieldElement multiply(EncryptionFieldElement b)
        {
            return new Fp(p, x.multiply(b.x).mod(p));
        }

        @Override
        public EncryptionFieldElement divide(EncryptionFieldElement b)
        {
            return new Fp(p, x.multiply(b.x.modInverse(p)).mod(p));
        }

        @Override
        public EncryptionFieldElement negate()
        {
            return new Fp(p, x.negate().mod(p));
        }

        @Override
        public EncryptionFieldElement square()
        {
            return new Fp(p, x.multiply(x).mod(p));
        }

        @Override
        public EncryptionFieldElement invert()
        {
            return new Fp(p, x.modInverse(p));
        }

        // D.1.4 91
        /**
         * return a sqrt root - the routine verifies that the calculation
         * returns the right value - if none exists it returns null.
         */

        @Override
        public EncryptionFieldElement sqrt() {
            // p mod 4 == 3
            if ( p.testBit(1) ) {
                // z = g^(u+1) + p, p = 4u + 3
                EncryptionFieldElement z = new Fp(p, x.modPow(p.shiftRight(2).add(ONE), p));

                return z.square().equals(this) ? z : null;
            }

            throw new RuntimeException("not done yet");
        }
    }
}
