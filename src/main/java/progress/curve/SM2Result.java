
package progress.curve;


import java.math.BigInteger;


/**
 * @Author: ERIC
 * @Date:2020/7/17-22:56
 * @Describe:SM2算法结果
 */
public class SM2Result {

	public SM2Result() {
	}


	/**签名、验签*/
	public BigInteger r;
	public BigInteger s;
	public BigInteger R;

	/**密钥交换*/
	public byte[] sa;
	public byte[] sb;
	public byte[] s1;
	public byte[] s2;


}