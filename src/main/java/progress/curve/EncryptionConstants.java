package progress.curve;

import java.math.BigInteger;

/**
 * @Author: ERIC
 * @Date:2020/7/17-22:56
 * @Describe:椭圆曲线的常量
 */
public interface EncryptionConstants {

    BigInteger ONE = BigInteger.valueOf(1);

}
