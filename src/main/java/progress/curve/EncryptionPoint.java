package progress.curve;

import java.math.BigInteger;

/**
 * @Author: ERIC
 * @Date:2020/7/17-22:56
 * @Describe:椭圆曲线的基点
 */
public abstract class EncryptionPoint {
    EncryptionCurve curve;
	EncryptionFieldElement x;
	EncryptionFieldElement y;

	protected EncryptionPoint(EncryptionCurve curve, EncryptionFieldElement x, EncryptionFieldElement y)
	{
        this.curve = curve;
		this.x = x;
		this.y = y;
	}
		
	public EncryptionFieldElement getX()
	{
		return x;
	}

	public EncryptionFieldElement getY()
	{
		return y;
	}

    @Override
    public boolean equals(Object  other) {
        if ( other == this ){
            return true;
        }
        if (!(other instanceof EncryptionPoint)){
            return false;
        }
        EncryptionPoint o = (EncryptionPoint)other;

        return x.equals(o.x) && y.equals(o.y);
    }

	public abstract byte[] getEncoded();
	public abstract EncryptionPoint add(EncryptionPoint b);
	public abstract EncryptionPoint subtract(EncryptionPoint b);
	public abstract EncryptionPoint twice();
	public abstract EncryptionPoint multiply(BigInteger b);

    /**
     * Elliptic curve points over Fp
     */
    public static class Fp extends EncryptionPoint {
        public Fp(EncryptionCurve curve, EncryptionFieldElement x, EncryptionFieldElement y) {
            super(curve, x, y);
        }

        /**
         * return the field element encoded with point compression. (S 4.3.6)
         */
        @Override
        public byte[] getEncoded() {
            byte    PC;

            if (this.getY().toBigInteger().testBit(0)) {
                PC = 0x02;
            } else {
                PC = 0x03;
            }

            byte[]  X = this.getX().toBigInteger().toByteArray();
            byte[]  PO = new byte[X.length + 1];

            PO[0] = PC;
            System.arraycopy(X, 0, PO, 1, X.length);

            return PO;
        }

        @Override
        public EncryptionPoint add(EncryptionPoint b) {
            EncryptionFieldElement gamma = b.y.subtract(y).divide(b.x.subtract(x));

            EncryptionFieldElement x3 = gamma.multiply(gamma).subtract(x).subtract(b.x);
            EncryptionFieldElement y3 = gamma.multiply(x.subtract(x3)).subtract(y);

            return new Fp(curve, x3, y3);
        }

        @Override
        public EncryptionPoint twice() {
            EncryptionFieldElement TWO = curve.fromBigInteger(BigInteger.valueOf(2));
            EncryptionFieldElement THREE = curve.fromBigInteger(BigInteger.valueOf(3));
            EncryptionFieldElement gamma = x.multiply(x).multiply(THREE).add(curve.a).divide(y.multiply(TWO));

            EncryptionFieldElement x3 = gamma.multiply(gamma).subtract(x.multiply(TWO));
            EncryptionFieldElement y3 = gamma.multiply(x.subtract(x3)).subtract(y);
                
            return new Fp(curve, x3, y3);
        }

        @Override
        public EncryptionPoint subtract(EncryptionPoint p2) {
            return add(new Fp(curve, p2.x, p2.y.negate()));
        }

        @Override
        public EncryptionPoint multiply(BigInteger k) {
            BigInteger e = k;

            BigInteger h = e.multiply(BigInteger.valueOf(3));

            EncryptionPoint R = this;

            for (int i = h.bitLength() - 2; i > 0; i--) {
                R = R.twice();       

                if ( h.testBit(i) && !e.testBit(i) ){
                    R = R.add(this);
                }else if ( !h.testBit(i) && e.testBit(i) ) {
                    R = R.subtract(this);
                }

            }


            return R;
        }
    }
}
