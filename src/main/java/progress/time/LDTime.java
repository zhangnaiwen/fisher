package progress.time;



import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class LDTime {

    public static void main(String[] args) {

        //获取当前时间戳
        String nowString = LocalDateTime.now().toString();
        System.out.println(nowString);

        //获取某个时区当前时间戳
        long time = System.currentTimeMillis();
        System.out.println(time);


        //自定义获取某个时区当前时间戳
        Long beijing = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();;
        System.out.println(beijing);
    }
}
