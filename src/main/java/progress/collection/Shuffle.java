package progress.collection;

import java.util.*;

public class Shuffle {

    public static void main(String[] args) {
        Integer[] ia = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        List<Integer> list = new ArrayList(Arrays.asList(ia));
        System.out.println("Before shufflig: " + list);
        Collections.shuffle(list);
        System.out.println("After shuffling: " + list);
      }
    }
