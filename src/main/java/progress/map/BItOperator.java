package progress.map;


/**
 * @Author: ZNW
 * @Date:2020/7/21-20:56
 */
public class BItOperator {

    public static void main(String[] args) {
        int  random = 15;
        boolean powerOfTwo = isPowerOfTwo(random);
        System.out.println(powerOfTwo);
    }

    /**
     * @Method:isPowerOfTwo
     * @Author: ZNW
     * @Date:2020/7/26-11:26
     * @Describe:判断一个整数是否是2的整数次幂的最快方法
     * @Params:[n]
     * @Return:boolean
     */
    private static boolean isPowerOfTwo(int n){
        return (n & (n-1))==0;
    }

}
