package progress.jsoup;





import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;
import java.io.IOException;

/**
 * @Classname HtmlToIPicture
 * @Description TODO
 * @Date 2020/12/15 14:32
 * @Created by 张乃文
 */
public class HtmlToPicture {
    public  static  String htmlString ;


    public static void main(String[] args) throws IOException {
        htmlString = FileUtils.readFileToString(new File("image/card1.html"));
        Document doc = Jsoup.parse(htmlString);
        for (int i = 0; i < 5; i++) {
            Element element = doc.select("span").get(i);
            String text = element.text();
            int index = text.indexOf("：");
            String prefix = text.substring(0, index+1);
            if(i == 0){
                String  result  = prefix + "中国移动集团";
                element.html(result);
            }else if(i == 1){
                String  result  = prefix + "17519469258";
                element.html(result);
            }else if(i == 2){
                String  result  = prefix + "220581199201171672";
                element.html(result);
            }else if(i == 3){
                String  result  = prefix +"中国移动";
                element.html(result);
            }else if(i == 4){
                String  result  = prefix + "2030-10-01";
                element.html(result);
            }
        }
        String html = doc.html();
    }
}
