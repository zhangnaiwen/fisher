package progress.jsoup;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.io.IOException;

/**
 * @Classname JsoupHtml
 * @Description TODO
 * @Date 2021/1/2 12:07
 * @Created by 张乃文
 */
public class JsoupHtml {
    @Test
    public  void test () throws IOException {

        final String url="http://www.baishixi.com/taiwan/juedaishuangjiaosuyoupengban/" ;

        Document doc = Jsoup.connect(url).get();
        Elements elements = doc.select("[id=downul]");

        for (Element elementLi : elements) {
            Elements provinceEl = elementLi.getElementsByTag("li");
            Elements elementBox = provinceEl.select("[type=checkbox]");

            for(Element inputTag : elementBox)
            {
                String value = inputTag.attr("value");
                System.out.println(value);
            }
        }


    }

}
