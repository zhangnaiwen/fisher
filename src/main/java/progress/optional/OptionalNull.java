package progress.optional;

import java.util.Optional;

public class OptionalNull {
    /**
     * <p>
     * 传统方式判断Null值
     * </p>
     */
      public void invoke(Object object){
          if(null == object){
              new Object();
          }
      }
     /**
     * <p>
     * 优雅方式判断Null值
     * </p>
     */
    public static void main(String[] args) {
        Optional.ofNullable(null).orElse(new Object());
    }
}
