package personal;


import org.junit.Test;

import java.io.File;

/**
 * @Classname PersonalFile
 * @Description TODO
 * @Date 2020/12/28 8:41
 * @Created by 张乃文
 */
public class PersonalFile {

   public static String path = "F:\\我的下载\\swag - ladyyuan";


   @Test
    public  void test() {
            File file = new File(path);
            String [] fileName = file.list();
            File[] files = file.listFiles();
           //updateIndex(fileName,files);
           //deleteSuffix(fileName,files);
           //deletePrefixNumber(fileName,files);
            deleteSeparator(fileName,files);
           //deleteSpecial(fileName,files);
              //recursiveFile(files);
    }
    /**
     * @Method:recursiveFile
     * @Author: ZNW
     * @Date:2021/1/6-13:38
     * @Describe:批量修改某文件夹下所有子文件夹下的文件（与其所在文件目录一致）
     * @Params:[files]
     * @Return:void
     */
    public static  void recursiveFile(File[] files){
       int x =0;
        for(File file:files) {
            boolean isDirectory = file.isDirectory();
            if(isDirectory){
                String parentName = file.getName();
                File subFile = new File(path+"\\"+parentName);
                File[] subFiles = subFile.listFiles();
                String[] subNames = subFile.list();

                for (int i = 0; i < subNames.length; i++) {
                    String subName = subFiles[i].getName();
                    //去除某文件夹下所有子文件夹的名称部分名字
                    String str = "";
                    //String name =   subName.replaceAll("\\"+"【"+str+"\\"+"】","");
                    //System.out.println(subName);
                    subFiles[i].renameTo(new File(subFile+"\\"+parentName+".xml"));
                      x++;
                }

            }

        }
        System.out.println(x);
    }

    /**
     * @Method:updateIndex
     * @Author: ZNW
     * @Date:2020/12/29-18:59
     * @Describe:修改前缀下标
     * @Params:[fileName, files]
     * @Return:void
     */
    public static  void updateIndex(String [] fileName,File[] files){
        for (int i = 0; i < fileName.length; i++) {
            if(fileName[i].indexOf(".")!=-1){
                int index = fileName[i].indexOf(".");
                String pk = fileName[i].substring(0, index);
                String suffix = fileName[i].substring(index);
                int prefix = Integer.parseInt(pk) + 156;
                String fName = prefix + suffix;
                System.out.println(fName);
                files[i].renameTo(new File(path+"\\"+fName));
            }
        }
    }

    /**
     * @Method:deletePrefixNumber
     * @Author: ZNW
     * @Date:2020/12/29-18:59
     * @Describe:删除前六位数字
     * @Params:[fileName, files]
     * @Return:void
     */
    public static  void deletePrefixNumber(String [] fileName,File[] files){
        for (int i = 0; i < fileName.length; i++) {

               String prefix = fileName[i].substring(0,3);
               String suffix = fileName[i].substring(12);
               String substring = prefix +suffix;
                System.out.println(substring);
                files[i].renameTo(new File(path+"\\"+substring));

        }
    }

    public static  void deleteSpecial(String [] fileName,File[] files){
        for(File file:files) {
            boolean isDirectory = file.isDirectory();
            String name = file.getName();
            if(isDirectory && name.indexOf("（")!=-1){
                int index = name.indexOf("（");
                String res = name.substring(0, index);
                file.renameTo(new File(path+"\\"+res+""));
            }
            if(isDirectory && name.indexOf("(")!=-1){
                int index = name.indexOf("(");
                String res = name.substring(0, index);
                file.renameTo(new File(path+"\\"+res+""));
            }
            if(isDirectory && name.indexOf("【")!=-1){
                int index = name.indexOf("【");
                String res = name.substring(0, index);
                file.renameTo(new File(path+"\\"+res+""));
            }
            if(isDirectory && name.indexOf("[")!=-1){
                int index = name.indexOf("[");
                String res = name.substring(0, index);
                file.renameTo(new File(path+"\\"+res+""));
            }
            if(isDirectory && name.indexOf("@")!=-1){
                int index = name.indexOf("@");
                String res = name.substring(0, index);
                file.renameTo(new File(path+"\\"+res+""));
            }

        }
    }
    public static  void deleteSeparator(String [] fileName,File[] files){
        for (int i = 0; i < fileName.length; i++) {

            if(fileName[i].indexOf("-")!=-1){
                int index = fileName[i].indexOf("-");
                String name = fileName[i].substring(0, index);
                files[i].renameTo(new File(path+"\\"+name+".mp4"));
            }
            if(fileName[i].indexOf("_")!=-1){
                int index = fileName[i].indexOf("_");
                String name = fileName[i].substring(0, index);
                files[i].renameTo(new File(path+"\\"+name+".mp4"));
            }

        }
    }

    /**
     * @Method:deleteSuffix
     * @Author: ZNW
     * @Date:2020/12/29-18:59
     * @Describe:删除下载信息里的视频号
     * @Params:[fileName, files]
     * @Return:void
     */
    public static  void deleteSuffix(String [] fileName,File[] files){

        for (int i = 0; i < fileName.length; i++) {
            boolean foo = fileName[i].contains("(Av");
            if(foo){
                int index = fileName[i].indexOf("A");
                String name = fileName[i].substring(0, index - 1);
                System.out.println(name);
                files[i].renameTo(new File(path+"\\"+name+".mp3"));
            }

        }
    }

}
