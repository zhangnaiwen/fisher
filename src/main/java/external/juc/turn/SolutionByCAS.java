package external.juc.turn;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Classname SolutionByCAS
 * @Description TODO
 * @Date 2021/1/21 19:23
 * @Created by 张乃文
 */
public class SolutionByCAS {

    public static void main(String[] args) throws Exception {
        SolutionByCAS solution = new SolutionByCAS();
        Thread t1 = new Thread(solution::printSmallByCAS);
        Thread t2 = new Thread(solution::printBigByCAS);
        t1.start();
        t2.start();
    }
    /****** CAS 乐观锁实现*/

    AtomicInteger flag = new AtomicInteger(1);


    public void printSmallByCAS() {
        for (char  i = 'a'; i <='z'; i++) {
            while (flag.get()!=1){}
            System.out.print(i);
            flag.set(0);
        }
    }
    public void printBigByCAS() {
        for (char  i = 'A'; i <='Z'; i++) {
            while (flag.get()==1) {}
            System.out.print(i);
            flag.set(1);
        }
    }
}
