package external.juc.turn;


import java.util.concurrent.locks.LockSupport;

/**
 * @Classname SolutionByPark
 * @Description TODO
 * @Date 2021/1/25 10:15
 * @Created by 张乃文
 */
public class SolutionByPark {
      static Thread t1 = null;
      static Thread t2 = null;


    public static void main(String[] args) {
          t1 = new Thread(()->{
              for (char  i = 'a'; i <='z'; i++) {
                  System.out.print(i);
                  LockSupport.unpark(t2);
                  LockSupport.park();

              }
          },"t1");
          t2 =  new Thread(()->{
              for (char  i = 'A'; i <='Z'; i++) {
                  LockSupport.park();
                  System.out.print(i);
                  LockSupport.unpark(t1);
              }
          },"t2");
          t1.start();
          t2.start();
    }

}
