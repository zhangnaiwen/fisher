package external.juc.turn;



/**
 * @Classname SolutionByVolatile
 * @Description TODO
 * @Date 2021/1/21 19:44
 * @Created by 张乃文
 */
public class SolutionByVolatile {
    public static void main(String[] args) throws Exception {
        SolutionByVolatile solution = new SolutionByVolatile();
        Thread t1 = new Thread(solution::printSmallByVolatile);
        Thread t2 = new Thread(solution::printBigByVolatile);
        t1.start();
        t2.start();
    }
    /****** volatile 关键字实现*/

    volatile boolean vflag = true;

    public void printSmallByVolatile() {
        for (char  i = 'a'; i <='z'; i++) {
            while (!vflag){}
            System.out.print(i);
            vflag = false;

        }
    }
    public void printBigByVolatile() {
        for (char  i = 'A'; i <='Z'; i++) {
            while (vflag){}
            System.out.print(i);
            vflag = true;

        }
    }
}
