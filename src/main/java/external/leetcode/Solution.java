package external.leetcode;


import com.google.common.collect.Maps;
import java.util.HashMap;

/**
 * @Classname Solution
 * @Description leetco两数之和，给定数组、目标值，返回和为目标值的两个数的下标
 * @Date 2021/1/23 17:27
 * @Created by 张乃文
 */
public class Solution {


    public  static void main(String[] args) {
        int[] arr = new int[]{2,9,11,16,22,33};
        int[] array = twoSum(arr, 20);
        for(int i = 0; i < array.length; i++){
            System.out.println("返回下标为:"+array[i]);
        }
    }
    public static int[] twoSum(int[] numbers, int target) {
        int [] res = new int[2];
        if(numbers==null||numbers.length<2){
            return res;
        }
        HashMap<Integer,Integer> map = Maps.newHashMap();
        for(int i = 0; i < numbers.length; i++){
            int temp = target-numbers[i];
            if(!map.containsKey(temp)){
                map.put(numbers[i],i);
            }else{
                res[0]= map.get(temp);
                res[1]= i;
                break;
            }
        }
        return res;
    }
}
