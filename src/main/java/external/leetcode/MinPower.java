package external.leetcode;

import org.junit.Test;

/**
 * @Classname NumberCheck
 * @Description TODO
 * @Date 2020/12/25 14:07
 * @Created by 张乃文
 */
public class MinPower {
    public int num = 7;

    /***位运算**/
    /***&  规则： 双1为1**/
    /***|  规则： 双0为0**/
    /***^  规则： 同0异1**/




    @Test
    public void checkNumber(){
        int i = num & (num - 1);
        if (i == 0) {
            System.out.println(num+"是2的幂次方");
        }else {
            System.out.println(num+"不是2的幂次方");
        }
    }
}
